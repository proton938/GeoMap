import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

import { HomeComponent } from './home/home.component';
import {DetailComponent} from './home/detail/detail.component';

const homeRoutes: Routes = [
  { path: 'detail', component: DetailComponent }
];

const routes: Routes = [
  { path: '', component: HomeComponent, children: homeRoutes }
];

@NgModule({
  imports: [BrowserModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

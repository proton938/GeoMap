import { Injectable } from '@angular/core';

@Injectable()

export class ApiParams {
	objPath: string = 'getObject';
	gridPath: string = 'getGrids';
}

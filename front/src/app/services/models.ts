import {Injectable} from '@angular/core';
import { Grid, ObjectParams } from './interfaces';

@Injectable()

export class Models {
  gridArray: Grid;
  objectBody: ObjectParams = {SquareTop: 0, SquareLeft: 0 };
}


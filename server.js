const express = require("express");
const fs = require("fs");
const app = express();
const jsonParser = express.json();


// обработчик по маршруту localhost:3000/api/getObjects
app.get("/api/getObjects", function(request, response){
       
	let dt = `${request.url}`;
    let answer = {id: 5, name: "Tom", url: dt};
	
	/*
	fs.readFile('base/objects.json', 'utf8', function (err, data) {
	if (err) throw err;
	  answer = JSON.parse(data);
	});
	*/
	answer = JSON.parse(fs.readFileSync('base/objects.json', 'utf8'));
     
    // настройка заголовков CORS для кроссдоменных запросов
    response.header('Access-Control-Allow-Origin', '*');
    response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    response.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
     
    response.send(answer);
});


app.get("/api/getGrids", function(request, response){
	
	let answer = JSON.parse(fs.readFileSync('base/grids/grid_1_1.json', 'utf8'));
     
    // настройка заголовков CORS для кроссдоменных запросов
    response.header('Access-Control-Allow-Origin', '*');
    response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    response.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
     
    response.send(answer);

});


// настройка CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, PATCH, PUT, POST, DELETE, OPTIONS");
    next();  // передаем обработку запроса методу app.post("/createobj"...
});

app.post("/createobj", jsonParser, function (request, response) {

    // если не переданы данные, возвращаем ошибку
    if(!request.body) return response.sendStatus(400);

    let objects = JSON.parse(fs.readFileSync("base/test.json", "utf8"));
    objects.Objects.push(request.body);
    console.log(request.body);
    console.log(objects.Objects.length);

    fs.writeFile("base/test.json", JSON.stringify(objects, null, '\t'), function(error){
        if(error) throw error; // если возникла ошибка
        console.log("Запись успешно завершена.");
    });

    response.json(JSON.stringify(request.body));
});


app.listen(2000);
